var express = require('express');
var router = express.Router();
var User = require('../models/user');

router.get('/', function (req, res, next) {
	return res.render('register.ejs');
});

router.post('/', function(req, res, next) {
	console.log(req.body);
	var userInfo = req.body;
	if(!userInfo.email || !userInfo.username || !userInfo.password || !userInfo.passwordConf){
		res.send();
	} else {
		if (userInfo.password == userInfo.passwordConf) {
			User.findOne({email:userInfo.email},function(err,data){
				if(!data){
					var c;
					User.findOne({},function(err,data){
						if (data) {
							id = data.unique_id + 1;
						}else{
							id=1;
						}
						var newUser = new User({
							unique_id:id,
							email:userInfo.email,
							username: userInfo.username,
							password: userInfo.password,
							passwordConf: userInfo.passwordConf
						});

						newUser.save(function(err, user){
							if(err)
								console.log(err);
							else
								console.log('Success');
						});

					})
					res.send({"Success":"You are regestered,You can login now."});
				}else{
					res.send({"Success":"Email is already used."});
				}

			});
		}else{
			res.send({"Success":"password is not matched"});
		}
	}
});

router.get('/login', function (req, res, next) {
	return res.render('login.ejs');
});

router.post('/login', function (req, res, next) {
	//console.log(req.body);
	User.findOne({email:req.body.email},function(err,data){
		if(data){
			if(data.password==req.body.password){
				req.session.userId = data.unique_id;
				res.send({"Success":"Success!"});
				
			}else{
				res.send({"Success":"Wrong password!"});
			}
		}else{
			res.send({"Success":"This Email Is not regestered!"});
		}
	});
});

router.get('/welcome', function (req, res, next) {
	User.findOne({unique_id:req.session.userId},function(err,data){
		if(!data){
			res.redirect('/');
		}else{
			return res.render('welcome.ejs', {"name":data.username,"email":data.email});
		}
	});
});

module.exports = router;